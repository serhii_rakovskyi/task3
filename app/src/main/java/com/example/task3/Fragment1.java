package com.example.task3;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class Fragment1 extends Fragment {

    TextView textView;
    TextView counTextView;

    public Fragment1() {
        // Required empty public constructor
    }

    public static Fragment1 newInstance(Bundle bundle) {
        Fragment1 fragment = new Fragment1();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment1, container, false);
        Bundle bundle = getArguments();

        textView = v.findViewById(R.id.Fragment);

        textView.setText(bundle.getString(App.SPManager.TEXT));
        counTextView = v.findViewById(R.id.Count);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        //sPref = this.getContext().getSharedPreferences(MainActivity.SPrefName,
        //       android.content.Context.MODE_PRIVATE);
        int Count = App.SPManager.getCount(textView.getText().toString())+1;
        counTextView.setText(Count+"");
        App.SPManager.saveCount(Count,textView.getText().toString());
        Log.d("task3", "onresume");
    }

}
