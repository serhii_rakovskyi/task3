package com.example.task3;

public interface SharedPreferencesManagerInterface {

    void saveCount(int count,String SPrefCount);
    int getCount(String SPrefCount);

}
