package com.example.task3;


import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesManager implements SharedPreferencesManagerInterface {
    SharedPreferences sPref;

    final static String TEXT = "text";
    final static String SPREF_NAME = "count_storage";

    SharedPreferencesManager(Context context) {
        sPref = context.getSharedPreferences(SPREF_NAME, context.MODE_PRIVATE);
    }

    @Override
    public void saveCount(int count, String SPrefCount) {
        SharedPreferences.Editor editor = sPref.edit();
        editor.putInt(SPrefCount, count);
        editor.apply();
    }

    @Override
    public int getCount(String SPrefCount) {
        return sPref.getInt(SPrefCount, 0);
    }


}

