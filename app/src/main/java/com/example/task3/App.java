package com.example.task3;

import android.app.Application;

public class App extends Application {
    public static SharedPreferencesManager SPManager;

    @Override
    public void onCreate() {
        super.onCreate();
        SPManager = new SharedPreferencesManager(this);
    }
}
